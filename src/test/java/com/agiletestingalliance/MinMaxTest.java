package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class MinMaxTest {

          @Test
          public void testMinMaxparam2() throws Exception {
                     int result = new MinMax().calccmp(2,4);
                     assertEquals("Param2 is greater",result, 4);
          }

          @Test
          public void testMinMaxparam1() throws Exception {
                     int result = new MinMax().calccmp(4,2);
                     assertEquals("Param1 is greater",result, 4);
          }
}

